# pict-rs-admin
_A demo application showing off pict-rs administration capibilities_

![Application](https://git.asonix.dog/asonix/pict-rs-admin/raw/branch/main/images/example.png)

### Note: pict-rs-admin is not intended to be exposed to the internet.

## Running
```bash
$ ./pict-rs-admin -h
Usage: pict-rs-admin [OPTIONS] --pict-rs-api-key <PICT_RS_API_KEY>

Options:
  -b, --bind-address <BIND_ADDRESS>
          [env: PICTRS_ADMIN__BIND_ADDRESS=] [default: 127.0.0.0:8084]
      --pict-rs-endpoint <PICT_RS_ENDPOINT>
          [env: PICTRS_ADMIN__PICTRS_ENDPOINT=] [default: http://localhost:8080]
      --pict-rs-api-key <PICT_RS_API_KEY>
          [env: PICTRS_ADMIN__PICTRS_API_KEY=]
      --opentelemetry-url <OPENTELEMETRY_URL>
          [env: PICTRS_ADMIN__OPENTELEMETRY_URL=]
      --opentelemetry-event-buffer-size <OPENTELEMETRY_EVENT_BUFFER_SIZE>
          [env: PICTRS_ADMIN__OPENTELEMETRY_EVENT_BUFFER_SIZE=]
  -h, --help
          Print help
```

## Contributing
Feel free to open issues for anything you find an issue with. Please note that any contributed code will be licensed under the AGPLv3.

## License

Copyright © 2023 asonix

pict-rs-admin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

pict-rs-admin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. This file is part of pict-rs.

You should have received a copy of the GNU General Public License along with pict-rs. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).
