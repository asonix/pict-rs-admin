{ lib
, nixosTests
, rustPlatform
}:

rustPlatform.buildRustPackage {
  pname = "pict-rs-admin";
  version = "0.2.0";
  src = ./.;
  cargoLock.lockFile = ./Cargo.lock;

  RUSTFLAGS = "--cfg tokio_unstable";

  nativeBuildInputs = [ ];

  passthru.tests = { inherit (nixosTests) pict-rs-admin; };

  meta = with lib; {
    description = "A simple image hosting service";
    homepage = "https://git.asonix.dog/asonix/pict-rs-admin";
    license = with licenses; [ agpl3Plus ];
  };
}
